import { Component, OnInit } from '@angular/core';
import { GamesDataService } from '../services/games-data.service';
import { GiantBombGames } from '../entities/giant-bomb-games';
import { ViewportScroller } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-group',
  templateUrl: './card-group.component.html',
  styleUrls: ['./card-group.component.scss']
})
export class CardGroupComponent implements OnInit {
  lastGamesRandom:GiantBombGames[];
  pageIndex:number=1;
  constructor(private GamesData:GamesDataService,private viewportScroller: ViewportScroller,private router:Router) { }

  ngOnInit() {
    if (this.router.url== "/") {
      this.lastGamesRandom = this.shuffle(this.GamesData.games); 
    } else {
      //console.log("platform",this.GamesData.gamePlatform.getValue());
        this.GamesData.gamePlatform.subscribe(games=>{
          this.lastGamesRandom = this.shuffle(games)
        })
     
    }
  
    //this.lastGamesRandom = this.lastGamesRandom.slice(Math.max(this.GamesData.games.length - 9, 1));
    //console.log("TCL: CardGroupComponent -> ngOnInit -> this.lastGamesRandom", this.lastGamesRandom)
  
    
  }
  shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
  }
  pagination(event) {
    this.pageIndex = event;
    this.viewportScroller.setOffset([-5,0])
    this.viewportScroller.scrollToAnchor("group-anchor");

  }

}
