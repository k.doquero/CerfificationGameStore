import { Component, OnInit } from '@angular/core';
import { CartDataService } from '../services/cart-data.service';
import { Router } from '@angular/router';
import { GameStoreApiService } from '../services/game-store-api.service';
import { FormGroup, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { ClientService } from '../services/client.service';
import { ClientInfo } from '../entities/client-info';
import { Client } from '../entities/client';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [FormBuilder]
})
export class NavbarComponent implements OnInit {
  totalItems: number = 0;
  total: number = 0;
  formSign: FormGroup;
  isLogged: Boolean = false;
  name: String = "";
  connected: Boolean = false;;
  isNotVerified:Boolean = false;
  constructor(private fb: FormBuilder, private cartService: CartDataService, private Router: Router, private gameStoreApi: GameStoreApiService,private clientService:ClientService) {
    this.formSign = fb.group({
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(2)]]
    })
    this.cartService.isTotalItem.subscribe(value => {
      this.totalItems = value;
    })
    this.cartService.isTotalPrice.subscribe(value => {
      this.total = value;
    })
    this.gameStoreApi.isloggedValidator.subscribe(value => {
      this.isLogged = value;


    })
    this.clientService.client.subscribe(client=>{
      this.name = client.name;
    })
    if (this.gameStoreApi.getTokenLogged()) {
      this.gameStoreApi.getClientById(this.gameStoreApi.getTokenLogged().id).subscribe(client=>{
        console.log("autolog",this.gameStoreApi.getTokenLogged());
          console.log(client);
          if (client.id) {
            this.logClient(client)
          }
       
      },error=>{console.log("error login",error);
      })
    }

  }

  ngOnInit() {
  }
  goToCart() {
    if (this.gameStoreApi.isLogged) {
      this.Router.navigate(["cart", this.clientService.getClient().id])
    } else {
      this.Router.navigate(["cart"]);
    }

  }
  login(id) {
    
  }
  logOut(){
    this.gameStoreApi.logout().subscribe(value=>{
      this.gameStoreApi.isLogged = false;
      this.gameStoreApi.setLogged(false);
      this.cartService.cartItems = [];
      this.gameStoreApi.setProducts([]);
      this.Router.navigate(['/']);
    })
  }
  onSubmit() {
    this.gameStoreApi.login(this.formSign.value,this.connected).subscribe(client => {
      console.log("tokenInfos",client);
     this.logClient(client);
     
    })

  }
  logClient(client:Client) {
    if (client.isVerified ==true) {
      this.isLogged = true;
      this.Router.navigate(['/']);
      this.gameStoreApi.setLogged(true);
    } else {
      this.isNotVerified = true;
    }
  }
  checkOnline(e) {
    if (e.target.checked) {
      this.connected = true;
    } else {
      this.connected = false;
    }
  }
  goToAccount() {
    this.Router.navigate(["account", this.clientService.getClient().id])
  }
}
