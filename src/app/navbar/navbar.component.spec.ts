import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarComponent } from './navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClientJsonpModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarComponent ],imports: [
        ReactiveFormsModule,HttpClientTestingModule,HttpClientJsonpModule,RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    
  });

  it('should have 0 ITEM INSIDE CARD', () => {
    const compiled = fixture.debugElement.nativeElement;
    
    expect(component.totalItems).toEqual(0);
    
    //expect(component).toBeTruthy();
  });
});
