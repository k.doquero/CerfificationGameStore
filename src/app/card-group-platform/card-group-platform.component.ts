import { Component, OnInit } from '@angular/core';
import { GamesDataService } from '../services/games-data.service';
import { Router } from '@angular/router';
import { GiantBombGames } from '../entities/giant-bomb-games';

@Component({
  selector: 'app-card-group-platform',
  templateUrl: './card-group-platform.component.html',
  styleUrls: ['./card-group-platform.component.scss']
})
export class CardGroupPlatformComponent implements OnInit {

  constructor(private GameDataService: GamesDataService, private router: Router) { }
  idPlatform: number = 0;
  lastGamesRandom: GiantBombGames[];
  
  ngOnInit() {
    this.GameDataService.gamePlatform.subscribe(value => {
      this.lastGamesRandom = this.shuffle(value);
      this.lastGamesRandom = this.lastGamesRandom.slice(Math.max(value.length - 3, 1));
      console.log(this.lastGamesRandom);
    })





  }
  private shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

}
