import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardGroupPlatformComponent } from './card-group-platform.component';
import { CardComponent } from '../card/card.component';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';

xdescribe('CardGroupPlatformComponent', () => {
  let component: CardGroupPlatformComponent;
  let fixture: ComponentFixture<CardGroupPlatformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule,HttpClientJsonpModule],
      declarations: [ CardGroupPlatformComponent,CardComponent ],
      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardGroupPlatformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
