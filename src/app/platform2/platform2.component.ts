import { Component, OnInit } from '@angular/core';
import { GamesDataService } from '../services/games-data.service';
import { GiantBombGames } from '../entities/giant-bomb-games';
import { Router } from '@angular/router';
import { GiantBomb } from '../entities/giant-bomb';

@Component({
  selector: 'app-platform2',
  templateUrl: './platform2.component.html',
  styleUrls: ['./platform2.component.scss']
})
export class Platform2Component implements OnInit {
  games: GiantBombGames[] = [];
  idPlatform: number = 0;
  name: string = "";
  constructor(private gameDataService: GamesDataService, private router: Router) { }

  ngOnInit() {
    console.log(this.router);
    switch (this.router.url) {
      case "/platform/ps4":
        this.name = "Playstation 4";
        this.idPlatform = 146;
        break;
      case "/platform/XboxOne":
        this.name = "Microsoft Xbox One";
        this.idPlatform = 145;
        break;
      case "/platform/NSwitch":
        this.name = "Nintendo Switch";
        this.idPlatform = 157;
        break;
      default:
        this.name = "Playstation 4";
        this.idPlatform = 146;
        break;
    }
    this.gameDataService.getgameByPlatform(this.idPlatform).subscribe(data => {
      let results: GiantBomb;
      results = data;
      this.gameDataService.gamePlatform.next(results.results);
    
      console.log("platform launch",results.results);
      
      //this.lastGames = results.results.slice(Math.max(results.results.length - 5, 1));
      //this.gameDataService.gamePlatform = results.results;
    })
  }
  ngAfterViewInit(){
    
  }

}
