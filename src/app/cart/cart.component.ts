import { Component, OnInit } from '@angular/core';
import { CartDataService } from '../services/cart-data.service';
import { GiantBombGames } from '../entities/giant-bomb-games';
import { GameStoreApiService } from '../services/game-store-api.service';
import { Router } from '@angular/router';
import { ClientService } from '../services/client.service';
import { ToBuy } from '../entities/to-buy';
import { Game } from '../entities/game';
import { Product } from '../entities/product';
import { NewGame } from '../entities/new-game';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  games: GiantBombGames[];
  total: number = 0;
  toBuys: ToBuy[] = [];
  constructor(private CartService: CartDataService, private gameStoreService: GameStoreApiService, private Router: Router, private clientService: ClientService) { }
  ngOnInit() {
    console.log();
    this.CartService.shoppingCart.subscribe(shoppingCart => {
      if (shoppingCart) {
        this.toBuys = shoppingCart.addToBuys
       this.total = this.countTotal(this.toBuys)
       console.log(this.total ,"total");
       this.CartService.isTotalPrice.next(parseInt(this.total.toFixed(2)))
      }
    })
  }
  private updateShoppingCart(game: ToBuy) {
    game.Products[0].idShoppingCart = this.CartService.getShoppingCart().id;
    this.gameStoreService.updateProduct(game.Products[0]).subscribe(shoppingCart => {
      this.CartService.setShoppingCart(shoppingCart)
    })
  }
  private removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject = {};

    for (var i in originalArray) {
      lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for (i in lookupObject) {
      newArray.push(lookupObject[i]);
    }
    return newArray;
  }
  addproduct() {
    if (this.clientService.getClient()) {
      this.Router.navigate(["payment"])
    } else {
      this.Router.navigate(["payment"])
      console.log("not connected");
      localStorage.setItem('itemsGameStore', JSON.stringify(this.games))

    }


  }
  deleteToBuy(toBuy: ToBuy) {
    toBuy.Products[0].toDelete = true;
    console.log(toBuy);
    this.gameStoreService.deleteToBuy(this.CartService.getShoppingCart().id,toBuy.id).subscribe(shoppingCart=>{

    this.CartService.setShoppingCart(shoppingCart);
    })
   


  }

  incDecGame(event, game: ToBuy) {
    console.log(event, game);
    game.Products[0].qty = game.qty
    this.updateShoppingCart(game)
  }
  deleteCart() {
    this.gameStoreService.deleteCart(this.CartService.cart.id).subscribe(value => {
      this.CartService.cartItems = [];
      this.gameStoreService.setProducts([]);
    })
  }
  countTotal(tobuys:ToBuy[]) {
    let price =0;
    tobuys.forEach(game=>{
       price += ((game.Products[0].price/100) * game.qty)

    })
    return price;
  }
}
