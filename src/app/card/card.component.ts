import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GiantBombGames } from '../entities/giant-bomb-games';
import { GamesDataService } from '../services/games-data.service';
import { CartDataService } from '../services/cart-data.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() game:GiantBombGames;
  id: string;
  name: string;
  price:number;
  showingPrice:string;
  databasePrice:number;
  constructor(private router:Router,private DataService:GamesDataService,private CartService:CartDataService) { 
    this.price = this.CartService.getRandomInt(250000,800000) /100;
    console.log(this.price);
    let euro = this.price /100;
    //console.log("TCL: CardComponent -> constructor -> euro", euro.toFixed(2));
    this.showingPrice = euro.toFixed(2);
    this.databasePrice = Math.floor(this.price);
    //console.log("TCL: CardComponent -> constructor -> databasePrice", this.databasePrice)
    
    //this.showingPrice = (this.price /100).toFixed(2).toLocaleString("fr-FR",{style:"curreny",currency:"EUR"});
    
  }
  add(){
    this.game.price = this.price;
    this.CartService.addProduct(this.game,this.databasePrice);

  }
  ngOnInit() {
  }
  ngAfterViewInit(){
    this.game.qty = 0;
  }
  goToDetails(){
    this.game.price = this.price;
    this.DataService.game = this.game;
    
    this.router.navigate(['details',this.game.id,this.game.name]);
    
    this.DataService.price = this.price;
  }
ngOnDestroy() {
    
 }
 
}
