import { Component, OnInit } from '@angular/core';
import { GiantBombGames } from '../entities/giant-bomb-games';
import { GiantBombApiService } from '../services/giant-bomb-api.service';
import { GiantBomb } from '../entities/giant-bomb';
import { GamesDataService } from '../services/games-data.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router, NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  games:GiantBombGames[] = [];
  total:  number= 0;
  showVideo:Boolean = true;
  constructor(private giantBombApi: GiantBombApiService,private gamesData:GamesDataService, private loaderService:NgxUiLoaderService,private router: Router) {
      this.router.events.subscribe((navigationValue:NavigationEnd)=>{
      //console.log("value router",navigationValue);
          if (navigationValue.url == "/") {
            this.showVideo = true;
          } else {
            this.showVideo = false;
            //console.log("router false video");
            
          }
      })
   }

  ngOnInit() {
   
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    //this.loaderService.start();
    this.games = this.gamesData.games
   
  }
  

}
