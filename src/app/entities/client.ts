import { ShoppingCart } from "./shopping-cart";

export interface Client {
    id?:Number;
    name:String;
    email:String;
    password?:String;
    avatar?:String;
    createdAt?:String;
    updatedAt?:String;
    isVerified?:Boolean;
    ShoppingCart?:ShoppingCart;
    sucess?:Boolean;
}
