import { Product } from "./product";
import { NewGame } from "./new-game";

export interface ToBuy {
    id?:Number;
    idProduct:Number;
    idShoppingCart:Number;
    qty?:number;
    createdAt?:Date;
    updatedAt?:Date;
    Products?:NewGame[];
    idGiantBomb?:string;
    decrement?:Boolean;
    increment?:Boolean;
}
