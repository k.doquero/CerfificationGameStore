import { GiantBombGames } from "./giant-bomb-games";

export interface NewCart {
    addToBuys:GiantBombGames[];
    clientId:number;
    id:number;
    total:number;
    createdAt:string;
    updatedAt:string;
}
