import { ToBuy } from "./to-buy";

export interface ShoppingCart {
    addToBuys?:ToBuy[];
    clientId:Number;
    id:Number;
    total:Number;
    createdAt:Date;
    updatedAt:Date;
   
}
