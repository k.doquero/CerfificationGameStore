import { Client } from "./client";
import { ShoppingCart } from "./shopping-cart";
export interface UserInfos {
    client : Client;
    shoppingCart:ShoppingCart;
}
