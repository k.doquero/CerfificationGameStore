export interface TokenInfos {
    id:Number;
    success:Boolean;
    token:string;
}
