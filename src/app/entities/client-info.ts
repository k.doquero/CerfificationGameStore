import { Client } from "./client";
import { ShoppingCart } from "./shopping-cart";

export interface ClientInfo {
    client : Client;
    shoppingCart:ShoppingCart;
}
