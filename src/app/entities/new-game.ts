export interface NewGame {
    idGiantBomb:string;
    name:string;
    deck:string;
    price:number;
    idShoppingCart:Number;
    image:string;
    decrement?:Boolean;
    increment?:Boolean;
    qty?:Number;
    toDelete?:Boolean;
}
