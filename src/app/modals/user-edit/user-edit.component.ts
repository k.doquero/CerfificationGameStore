import { Component, OnInit, Input } from '@angular/core';
import { NgxSmartModalService,NgxSmartModalComponent} from 'ngx-smart-modal';
import { Client } from 'src/app/entities/client';
@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})


export class UserEditComponent implements OnInit {
  @Input() accountChangeModal:NgxSmartModalComponent;
  client:Client;
  constructor(public ngxSmartModalService: NgxSmartModalService) {
     
   }
   
  ngOnInit() {
    console.log(this.accountChangeModal);
    this.accountChangeModal.onDataAdded.subscribe(client=>{
     
      this.client = client
      console.log("inside",client,"this.client",this.client);
    })
    
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
   
  }
  save() {
    
    
    
  }

}
