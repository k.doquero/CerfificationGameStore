import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { tap, map, mergeMap, catchError } from 'rxjs/operators';
import { Client } from '../entities/client';
import { Observable, BehaviorSubject } from 'rxjs';
import { Cart } from '../entities/cart';
import { GiantBombGames } from '../entities/giant-bomb-games';
import { environment } from 'src/environments/environment';
import { UserInfos } from '../entities/user-infos';
import { ClientService } from './client.service';
import { ClientInfo } from '../entities/client-info';
import { TokenInfos } from '../entities/token-infos';
import { CartDataService } from './cart-data.service';
import { NewGame } from '../entities/new-game';
import { ShoppingCart } from '../entities/shopping-cart';


@Injectable({
  providedIn: 'root'
})
export class GameStoreApiService {

  apiDomain: string = environment.apiBackEnd + "/api/"
  private token: string = "";
  responseLoginData: string;
  cart: Object;
  private client: Client;
  isLogged: Boolean = false;
  isloggedValidator: BehaviorSubject<boolean> = new BehaviorSubject(false);
  productList: BehaviorSubject<GiantBombGames[]> = new BehaviorSubject([]);

  constructor(private http: HttpClient, private clientService: ClientService,private CartDataService:CartDataService) {
    this.CartDataService.newGame.subscribe(newGame=>{
      if (newGame && newGame.idShoppingCart !=0) {
          this.updateProduct(newGame).subscribe(value=>{this.CartDataService.setShoppingCart(value)
          })
      }
    })
   }


  setHttpOptions() {
    let head = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.getToken()
    })
    let httpOptions = {
      headers: head,
      withCredentials: true,
      credentials: 'include'
    };
    return httpOptions;
  }
   getToken():string {
    return this.token;
  }
  setToken(tokenInfo:TokenInfos) {
    console.log(tokenInfo);
    localStorage.setItem('tokenGameStore', JSON.stringify(tokenInfo));
    this.token = tokenInfo.token;
  }
  
  setProducts(products) {
    this.productList.next(products)
  }
  setTokenNoRemenber(tokenInfo:TokenInfos) {
    this.token = tokenInfo.token;
  }
  getTokenLogged() {
    let tokenInfos:TokenInfos = JSON.parse(localStorage.getItem("tokenGameStore"));
    if (tokenInfos) {
      this.token = tokenInfos.token;
    }
    return tokenInfos;
  }
  setLogged(value: boolean) {
    this.isloggedValidator.next(value)
  }
  getLogged():boolean {
   return  this.isloggedValidator.getValue()
  }
  newClient(client) {
    return this.http.post<Client>(`${this.apiDomain}clients`, client)
  }
  
  login(client, remember: Boolean) {
    return this.http.post<TokenInfos>(`${this.apiDomain}auth/login`, client).pipe(
      catchError(error=>{throw {error:error,message:"can't login user"}}),
      mergeMap((tokenInfo:TokenInfos) => {
        //this.clientService.setClient(cl)
        console.log("client info", tokenInfo, "connected", remember);
        if (remember === true) {
          this.setToken(tokenInfo)
        } else {
          this.setTokenNoRemenber(tokenInfo)
        }
        return this.getClientById(tokenInfo.id)
      })
    );

  }
  getClientById(id: Number) {

    return this.http.get<Client>(`${this.apiDomain}clients/${id}`, this.setHttpOptions()).pipe(
      map(clientInfo => {
        if (!clientInfo.id) return clientInfo;
      this.clientService.setClient(clientInfo);
      //this.setClient(clientInfo.client);
      if (clientInfo.isVerified) {
        this.setLogged(true);

        //this.CartDataService.upTotalItems(this.CartDataService.getShoppingCart().addToBuys.length +1)
      }
      return clientInfo;
    }))
  }
  updateClient(clientData: Client) {
    return this.http.patch(`${this.apiDomain}clients`, clientData,this.setHttpOptions())

  }
  logout() {
    return this.http.get(`${this.apiDomain}auth/logout`,this.setHttpOptions()).pipe(
      tap(value => {
        localStorage.removeItem('tokenGameStore');
        console.log("logout",value);

        this.token = null;
      })
    )
  }

  getCartWithProducts(clientId) {

    return this.http.get<Cart>(`${this.apiDomain}shoppingCarts/${clientId}?filter={"include":["products","client"]}&access_token=${this.getToken()}`)


  }
  createCart(clientId) {
    return this.http.get(`${this.apiDomain}clients/${clientId}/shoppingCarts?access_token=${this.getToken()}`)

  }

  updateProduct(product:NewGame) {
    return this.http.post<ShoppingCart>(`${this.apiDomain}products`, product,this.setHttpOptions())
  }

  deleteToBuy(idCart, idTobuy) {
    return this.http.delete<ShoppingCart>(`${this.apiDomain}cart/${idCart}/tobuy/${idTobuy}`,this.setHttpOptions())
  }
  deleteCart(idCart) {
    return this.http.delete(`${this.apiDomain}shoppingCarts/${idCart}/products?access_token=${this.getToken()}`)
  }
}


