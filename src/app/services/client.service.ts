import { Injectable } from '@angular/core';
import { Client } from '../entities/client';
import { BehaviorSubject } from 'rxjs';
import { CartDataService } from './cart-data.service';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  client:BehaviorSubject<Client> = new BehaviorSubject({ name:"",email:""})

  constructor(private cartService:CartDataService) { }
  setClient(client:Client){
    this.cartService.setShoppingCart(client.ShoppingCart)
    this.client.next(client)
  }
  getClient():Client{
    return this.client.getValue()
  }

}
