import { TestBed, inject, getTestBed } from '@angular/core/testing';

import { GameStoreApiService } from './game-store-api.service';
import { environment } from 'src/environments/environment';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
describe('GameStoreApiService', () => {
  let injector:TestBed;
  let service:GameStoreApiService;
  let httpMock:HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,HttpClientJsonpModule],
      providers: [GameStoreApiService]
    });
    injector = getTestBed()
    service = injector.get(GameStoreApiService)
    httpMock = injector.get(HttpTestingController)
  });
  afterEach(() => {
    httpMock.verify();
  });
  // it('should login', () => {
  //   let dummyClient = {email:environment.testLogin,password:environment.testPassword};
  //   service.login(dummyClient,true).subscribe(clientInfo=>{
  //     expect(clientInfo.id).toEqual(jasmine.any(Number));
     
  //   })
  
    
  // });
  it('should return a string', inject([GameStoreApiService], (service: GameStoreApiService) => {
      expect(service.getToken()).toEqual(jasmine.any(String));
  }));
});
describe("login",()=>{
  
})
