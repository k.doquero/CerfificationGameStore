import { Injectable } from '@angular/core';
import { Game } from '../entities/game';
import { GiantBombGames } from '../entities/giant-bomb-games';
import { GamesDataService } from './games-data.service';
import { BehaviorSubject } from 'rxjs';
import { Cart } from '../entities/cart';
import { GameStoreApiService } from './game-store-api.service';
import { ShoppingCart } from '../entities/shopping-cart';
import { ClientService } from './client.service';
import { ToBuy } from '../entities/to-buy';
import { NewGame } from '../entities/new-game';

@Injectable({
  providedIn: 'root'
})
export class CartDataService {
  games: GiantBombGames[];
  newGame: BehaviorSubject<NewGame> = new BehaviorSubject(null);
  cartItems: GiantBombGames[] = [];
  totalItems: number = 0;
  total: number = 0;
  isTotalItem: BehaviorSubject<number> = new BehaviorSubject(0);
  isTotalPrice: BehaviorSubject<number> = new BehaviorSubject(0);
  cart: Cart;
  shoppingCart: BehaviorSubject<ShoppingCart> = new BehaviorSubject(null);
  constructor(private GameData: GamesDataService) {
    this.games = this.GameData.games
    
  }
  setShoppingCart(shoppingCart: ShoppingCart) {
    this.isTotalItem.next(shoppingCart.addToBuys.length)
    this.shoppingCart.next(shoppingCart)
  }
  getShoppingCart() {
    return this.shoppingCart.getValue();
  }
  getNewGameToAdd() {
    return this.newGame.getValue()
  }
  setNewGameToAdd(game: NewGame) {
    this.newGame.next(game)
  }
  newToBuy(game: GiantBombGames) {

  }
  imageToB64() {
    const toDataURL = url => fetch(url)
      .then(response => response.blob())
      .then(blob => new Promise<string>((resolve, reject) => {
        const reader = new FileReader()
        reader.onloadend = () => resolve(reader.result.toString())
        reader.onerror = reject
        reader.readAsDataURL(blob)
      }))
    return toDataURL
  }
  async add(game: GiantBombGames,price) {
    this.cartItems.push(game);
  //  let tranform= await this.imageToB64()
  //   let  imageB64 = await tranform(game.image["original_url"]);
    let id;
    if (this.getShoppingCart()) {
      id = this.getShoppingCart().id
    } else {
      id=0;
    }
    let newGame: NewGame = {
      idGiantBomb: game.guid, name: game.name, deck: game.deck, price: price, idShoppingCart: id, image: game.image["original_url"]
    }
    console.log("TCL: CartDataService -> add -> newGame", newGame)
    this.setNewGameToAdd(newGame)
    return newGame;
  }
  totalPrice() {
    this.total += this.getRandomInt(2500, 8000);

  }
  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  upTotalItems(item: number) {
    this.isTotalItem.next(item)
  }
  uptotalPrice(price) {
    this.isTotalPrice.next(price)
  }
  addProduct(game, price) {
    this.add(game,price);
    this.total += price;
    this.totalItems++;
    this.uptotalPrice(this.total)
    //this.upTotalItems(this.getShoppingCart().addToBuys.length +1)
  }
}
