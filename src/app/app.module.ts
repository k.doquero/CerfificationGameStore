import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { UiModule } from './ui/ui.module';
import { SafePipe } from './pipes/safe.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { NgxStripeModule } from 'ngx-stripe';
import { HomepageComponent } from './homepage/homepage.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AccessbarComponent } from './accessbar/accessbar.component';
import { LastReleasedComponent } from './last-released/last-released.component';
import { CardGroupComponent } from './card-group/card-group.component';
import { CardComponent } from './card/card.component';
import { RssReaderComponent } from './rss-reader/rss-reader.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { VideoComponent } from './video/video.component';
import { DetailsComponent } from './details/details.component';
import { PlatformComponent } from './platform/platform.component';
import { Platform1Component } from './platform1/platform1.component';
import { Platform2Component } from './platform2/platform2.component';
import { Platform3Component } from './platform3/platform3.component';
import { Platform4Component } from './platform4/platform4.component';
import { CartComponent } from './cart/cart.component';
import { AccountComponent } from './account/account.component';
import { CardGroupPlatformComponent } from './card-group-platform/card-group-platform.component';
import { StripePayementComponent } from './stripe-payement/stripe-payement.component';
import { environment } from 'src/environments/environment';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
import { CentsToEuroPipe } from './pipes/cents-to-euro.pipe';
import {NgxPaginationModule} from 'ngx-pagination';
import { FooterComponent } from './footer/footer.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { UserEditComponent } from './modals/user-edit/user-edit.component';
import { LoginComponent } from './login/login.component';
@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    NavbarComponent,
    AccessbarComponent,
    LastReleasedComponent,
    CardGroupComponent,
    CardComponent,
    RssReaderComponent,
    SignUpComponent,
    VideoComponent,
    DetailsComponent,
    PlatformComponent,
    Platform1Component,
    Platform2Component,
    Platform3Component,
    Platform4Component,
    CartComponent,
    AccountComponent,
    CardGroupPlatformComponent,
    StripePayementComponent,
    SafePipe,
    CentsToEuroPipe,
    FooterComponent,
    UserEditComponent,
    LoginComponent,
    
  ],
  imports: [
    CommonModule,
    BrowserModule, HttpClientModule, HttpClientJsonpModule,
    UiModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgxStripeModule.forRoot(environment.stripeKey),
    NgxUiLoaderModule,
    NgxPaginationModule,
    NgxSmartModalModule.forRoot()
    
  ],
  providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
