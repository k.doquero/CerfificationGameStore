import {
  Component,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef, OnInit
} from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from "@angular/forms";
import { StripeService, Elements, Element as StripeElement, ElementsOptions } from "ngx-stripe";
import { CartDataService } from '../services/cart-data.service';
declare var Stripe: any;
@Component({
  selector: 'app-stripe-payement',
  templateUrl: './stripe-payement.component.html',
  styleUrls: ['./stripe-payement.component.scss']
})
export class StripePayementComponent implements AfterViewInit {
  elements: Elements;
  card: StripeElement;
  error:Boolean = false;
  success:Boolean = false;
  elementsOptions: ElementsOptions = {
    locale: 'fr'
  };

  stripeTest: FormGroup;
  total:number =0;
  constructor(
    private fb: FormBuilder,
    private stripeService: StripeService,private cartService:CartDataService) { }
    
  ngOnInit() {
    this.cartService.isTotalPrice.subscribe(value=> this.total =value)
    this.stripeTest = this.fb.group({
      name: ['', [Validators.required]]
    });
    this.stripeService.elements(this.elementsOptions)
      .subscribe(elements => {
        this.elements = elements;
        // Only mount the element the first time
        if (!this.card) {
          this.card = this.elements.create('card', {
            style: {
              base: {
                iconColor: '#c4f0ff',
                color: '#fff',
                fontWeight: 500,
                fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
                fontSize: '16px',
                fontSmoothing: 'antialiased',
        
                ':-webkit-autofill': {
                  color: '#fce883',
                },
                '::placeholder': {
                  color: '#87BBFD',
                },
              },
              invalid: {
                iconColor: '#FFC7EE',
                color: '#FFC7EE',
              }
            }
          });
          this.card.mount('#card-element');
        }
      });
  }
  ngAfterViewInit(){}
  buy() {
    const name = this.stripeTest.get('name').value;
    this.stripeService
      .createToken(this.card, { name })
      .subscribe(result => {
        if (result.token) {
          this.success = true;
          // Use the token to create a charge or a customer
          // https://stripe.com/docs/charges
          console.log(result.token);
        } else if (result.error) {
          // Error creating the token
          this.error = true;
          console.log(result.error.message);
        }
      });
  }
}
