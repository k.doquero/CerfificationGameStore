import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StripePayementComponent } from './stripe-payement.component';

xdescribe('StripePayementComponent', () => {
  let component: StripePayementComponent;
  let fixture: ComponentFixture<StripePayementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StripePayementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StripePayementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
