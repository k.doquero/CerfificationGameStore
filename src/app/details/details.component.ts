import { Component, OnInit } from '@angular/core';
import { GamesDataService } from '../services/games-data.service';
import { GiantBombGames } from '../entities/giant-bomb-games';
import { Router, ActivatedRoute } from '@angular/router';
import { GiantBombApiService } from '../services/giant-bomb-api.service';
import { CartDataService } from '../services/cart-data.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  game: GiantBombGames;
  constructor(private DataService: GamesDataService, private route: ActivatedRoute, private GiantBombApi: GiantBombApiService,private cartService:CartDataService) {
    
    if (this.DataService.game) {
      this.game= this.DataService.game
    } else {
      this.GiantBombApi.getById(this.route.snapshot.params.id).subscribe(game => {
      this.game = game.results;
      console.log(this.game);
      
    });
    }
    

    

  }

  ngOnInit() {
     
  }
  add(){
    this.cartService.addProduct(this.game,this.game.price)
  }

}
