import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AccessbarComponent } from './accessbar/accessbar.component';
import { LastReleasedComponent } from './last-released/last-released.component';
import { CardGroupComponent } from './card-group/card-group.component';
import { CardComponent } from './card/card.component';
import { RssReaderComponent } from './rss-reader/rss-reader.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { VideoComponent } from './video/video.component';
import { DetailsComponent } from './details/details.component';
import { PlatformComponent } from './platform/platform.component';
import { Platform1Component } from './platform1/platform1.component';
import { Platform2Component } from './platform2/platform2.component';
import { Platform3Component } from './platform3/platform3.component';
import { Platform4Component } from './platform4/platform4.component';
import { CartComponent } from './cart/cart.component';
import { AccountComponent } from './account/account.component';
import { CardGroupPlatformComponent } from './card-group-platform/card-group-platform.component';
import { StripePayementComponent } from './stripe-payement/stripe-payement.component';
import { LoggedGuard } from './guard/logged.guard';

export const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'home', component: HomepageComponent },
  { path: 'register', component: SignUpComponent },
  { path: 'details/:id/:name', component: DetailsComponent },
  { path: 'platform/ps4', component: Platform2Component },
  { path: 'platform/XboxOne', component: Platform2Component },
  { path: 'platform/Psvita', component: Platform2Component },
  { path: 'platform/NSwitch', component: Platform2Component },
  { path: 'cart', component: CartComponent },
  { path: 'cart/:id', component: CartComponent },
  { path: 'account/:id', component: AccountComponent,canActivate: [
    LoggedGuard
]},
  { path: 'payment', component: StripePayementComponent }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  declarations: [
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
