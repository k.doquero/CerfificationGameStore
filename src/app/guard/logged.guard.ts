import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { GameStoreApiService } from '../services/game-store-api.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedGuard implements CanActivate {
  constructor(private gameStoreApi: GameStoreApiService, private router: Router) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      
      
    const isSignedIn = this.gameStoreApi.getLogged()
    console.log(isSignedIn,"guard");
    if (!isSignedIn) {
      this.router.navigate(["/"])
    }
    return isSignedIn
  }
}
